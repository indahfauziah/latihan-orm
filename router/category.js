const express = require('express') 
const router = express.Router() 
const { list, create, update, destroy } = require('../controllers/categoryController') 
const validate = require('../middleware/validate')
const { createCategoryRules } = require('../validators/rule')
const checkToken = require('../middleware/checkToken')

router.get('/list', checkToken, list) 
router.post('/create', checkToken, validate(createCategoryRules), create) 
router.put('/update', checkToken, update) 
router.delete('/destroy', checkToken, destroy) 

module.exports = router 