require('dotenv').config() 
const express = require('express') 
const port = process.env.PORT || 3500 
const app = express() 
const cors = require('cors') 
const bodyParser = require('body-parser') 
const router = require('./router') 

app.use(cors()) 
app.use(express.urlencoded({ extended: true })) 
app.use(bodyParser.json()) 
app.use('/api', router) 

app.listen(port, () => {
    console.log(`server running at port ${port}`)
}) 